import 'bootstrap';
import './main.scss';

import { ImageLoader } from './classes/ImageLoader';
import { NavBarHandler } from './classes/NavBarHandler';

const imageLoader = new ImageLoader();
const navBarHandler = new NavBarHandler();

navBarHandler.checkScrollTop();