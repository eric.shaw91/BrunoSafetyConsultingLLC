export class ImageLoader {
	private images: HTMLAnchorElement[];

	constructor() {
		this.images =  [].slice.call(document.querySelectorAll('.img-source'));

		this.images.forEach((img) => {
			this.loadFullImage(img);
		});
	}

	loadFullImage(item : HTMLAnchorElement) : void {
		const img = new Image();
		const that = this;
		img.src = item.href;
		img.className = 'replace';
		if (img.complete) {
			that.phaseInImg(item, img);
		} else {
			img.addEventListener('load', function fullImageLoaded() {
				that.phaseInImg(item, img);
				img.removeEventListener('load', fullImageLoaded);
			})
		}
	}

	phaseInImg(item: HTMLAnchorElement, img: HTMLImageElement) : void {
		this.removePreviewFeatures(item);
		item
			.appendChild(img)
			.addEventListener('animationend', e => this.phaseOutPreview(e, item));
	}

	phaseOutPreview(e : Event, item: HTMLAnchorElement) {
		let previewImage = item.querySelector('.img-placeholder');
		let target: HTMLElement = <HTMLElement>e.target;
		item.removeChild(previewImage);
		target.classList.remove('replace');
		target.removeEventListener('animationend', e => this.phaseOutPreview(e, item));
	}
 
	/* removes the default behavior of an <a> element */
	removePreviewFeatures(item: HTMLAnchorElement) : void {
		item.classList.remove('replace');
		item.addEventListener('click', function(e) {
			e.preventDefault();
		})
	}
}