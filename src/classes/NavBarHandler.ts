import * as $ from "jquery";

const $window : JQuery<HTMLElement> = $(window);
const $navbar : JQuery<HTMLElement> = $(".navbar.fixed-top");
const $toggler : JQuery<HTMLElement> = $(".navbar-toggler");

export class NavBarHandler {

	constructor() {
		$window.scroll(this.handleScroll);
		$window.click(this.handleWindowClick);
		$toggler.click(this.handleToggleClick);
	}

	handleScroll() : void {
		if($window.scrollTop() > 300) {
			$navbar.addClass('opaque');
		} else {
			$navbar.removeClass('opaque');
		}
	}

	handleToggleClick() : void {
		if($window.scrollTop() < 300) {
			$navbar.toggleClass('opaque');
		}
	}

	handleWindowClick(e: any) : void {
		let target: JQuery<HTMLElement> = $(<HTMLElement>e.target);
		if(target.hasClass("nav-link")) {
			$(".navbar-collapse").collapse('hide');
		}
	}

	checkScrollTop() : void {
		this.handleScroll();
	}
}